<?php

class RemoveKeywordSimpleTest extends UnitTestCase {

 	private $basicArgs = array("filter_value" => "Linux", 
 							   "filter_match_case" => "0", 
 							   "filter_match_word" => "0",
 							   "filter_search_title" => "0",
 							   "filter_search_content" => "0",
 							   "filter_search_excerpt" => "0");
	private $post = array("post_content" => "Linux linux Linuxversie", 
						  "post_title" => "Linux linux Linuxversie",
						  "post_excerpt" => "Linux linux Linuxversie"); 

 public function testDefaultReturnNoChanges() {
        $f = new faf_remove_keywords($this->post,$this->basicArgs); 
        $p = $f->execute();
        $this->assertEqual($this->post, $p);
    }
 
 public function testMatchCase()
 {
        $args = $this->basicArgs;
        $args["filter_match_case"] = 1;
        $args["filter_search_title"] = 1;
        
        $f = new faf_remove_keywords($this->post,$args); 
        
        $title_filter = " linux versie"; 
        $p = $f->execute();
 		$this->assertEqual($title_filter,$p["post_title"]);
 		

 }   
 
 public function testMatchFullWord()
   {
  	 	$args = $this->basicArgs;
    	$args["filter_match_word"] = 1; 
        $args["filter_search_title"] = 1;  	
 	    $f = new faf_remove_keywords($this->post,$args); 	
 	    $title_filter = "  Linuxversie"; 
        $p = $f->execute();
 		$this->assertEqual($title_filter,$p["post_title"]);
   }
   
 public function testMatchSpecialCharacters()
 {
   	 	$args = $this->basicArgs;
        $args["filter_search_title"] = 1; 
        $args["filter_value"] = "–";
        $post = $this->post;
        $post["post_title"]  = "ZOETERMEER –";
  	    $f = new faf_remove_keywords($post,$args); 
        
        $title_filter = "ZOETERMEER ";
        $p = $f->execute();
  		$this->assertEqual($title_filter,$p["post_title"]);  
  		
		$args["filter_value"] = "&#8211;";
 		$post["post_title"] = "ZOETERMEER &#8211;";
 		$title_filter = "ZOETERMEER ";
  	    $f = new faf_remove_keywords($post,$args); 
        $p = $f->execute(); 
 		$this->assertEqual($title_filter,$p["post_title"]);      
        
		$args["filter_value"] = "ZOETERMEER -";
 		$post["post_title"] = "ZOETERMEER -";
 		$title_filter = "";        
  	    $f = new faf_remove_keywords($post,$args); 
        $p = $f->execute(); 
 		$this->assertEqual($title_filter,$p["post_title"]);      
        
 }  
 
 public function testMatchFullWordWithSpacingCharacter()
 {
    	 $args = $this->basicArgs;
      	$args["filter_match_word"] = 1;  	 	
        $args["filter_search_title"] = 1;
         
        $post = $this->post;
		$post["post_title"] = "Linux Linux-versie Linuxversie";
  	    
  	    $f = new faf_remove_keywords($post,$args); 
        
		$title_filter =  " -versie Linuxversie"; 
		$p = $f->execute();
		 
  		$this->assertEqual($title_filter,$p["post_title"]);    
 }   
 
   
}

?>
