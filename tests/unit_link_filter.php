<?php

class LinkFilterSimpleTest extends UnitTestCase {

 	private $basicArgs = array("filter_search_content" => "0",
 							   "filter_search_excerpt" => "0",
 							   "filter_link_blank" => "0",
 							   "filter_link_tracker" => ""
 							   );
	private $post = array("post_content" => "K<a href='http://www.bla.com/bla'>Blablabla</a>K", 
						  "post_title" => "Emtpy",
						  "post_excerpt" => "<a href='http://www.bla.com/bla'>Blablabla</a>"); 
	
	protected static function getMethod($name) {
	  $class = new ReflectionClass('faf_link_filter');
	  $method = $class->getMethod($name);
	  $method->setAccessible(true);
	  return $method;
	}
	
 public function testDefaultReturnNoChanges() {
        $f = new faf_link_filter($this->post,$this->basicArgs); 
        $p = $f->execute();
        $this->assertEqual($this->post, $p);
    }
 
 public function testLinkBlankFilter()
 {
        $args = $this->basicArgs;
        $args["filter_search_content"] = 1;
        $args["filter_link_blank"] = 1;
   		// $method = $this->getMethod("image_process");       
       
        
        // basic check on content
 		$f = new faf_link_filter($this->post,$args); 
        $expect = "K<a href=\"http://www.bla.com/bla\" target=\"_blank\">Blablabla</a>K"; 
        $p = $f->execute();
 		$this->assertEqual($expect, $p["post_content"]);
 		// test if not messed with other fields
 		$this->assertEqual($this->post["post_excerpt"],$p["post_excerpt"]);
 		
 		
 		// basic check on excerpt
 		$args["filter_search_content"] = 0; 
 		$args["filter_search_excerpt"] = 1;
 		$f = new faf_link_filter($this->post,$args); 

        $expect = "<a href=\"http://www.bla.com/bla\" target=\"_blank\">Blablabla</a>"; 
        $p = $f->execute();

 		$this->assertEqual($expect,$p["post_excerpt"]);
 		$this->assertEqual($this->post["post_content"], $p["post_content"]); 		

		// check for not doing job 
		$args["filter_search_content"] = 1; 
		$content = $this->post; 
		$content["post_content"] = "Failty content no link";  
		$f = new faf_link_filter($content,$args); 
        $p = $f->execute();

		$this->assertEqual($content["post_content"],$p["post_content"]); 		
 }   
 
 function testLinkTrackerFilter()
 {
 
        $args = $this->basicArgs;
        $args["filter_search_content"] = 1;
        $args["filter_link_tracker"] = "http://www.tracker.com/url=";
   		// $method = $this->getMethod("image_process");       
       
        
        // basic check on content
 		$f = new faf_link_filter($this->post,$args); 
        $expect = "K<a href=\"http://www.tracker.com/url=http://www.bla.com/bla\">Blablabla</a>K"; 
        $p = $f->execute();
 		$this->assertEqual($expect, $p["post_content"]);
 		// test if not messed with other fields
 		$this->assertEqual($this->post["post_excerpt"],$p["post_excerpt"]);
 
  		// basic check on excerpt
 		$args["filter_search_content"] = 0; 
 		$args["filter_search_excerpt"] = 1;
 		$f = new faf_link_filter($this->post,$args); 

        $expect = "<a href=\"http://www.tracker.com/url=http://www.bla.com/bla\">Blablabla</a>"; 
        $p = $f->execute();

 		$this->assertEqual($expect,$p["post_excerpt"]);
 		$this->assertEqual($this->post["post_content"], $p["post_content"]); 		

		// check for not doing job 
		$args["filter_search_content"] = 1; 
		$content = $this->post; 
		$content["post_content"] = "Failty content no link";  
		$f = new faf_link_filter($content,$args); 
        $p = $f->execute();

		$this->assertEqual($content["post_content"],$p["post_content"]); 	
 		
 }
 
 function testSaveHTML()
 {
 	$f = new faf_link_filter($this->post,$this->basicArgs); 
 	$loadhtml = $this->getMethod("load_content");
 	$savehtml = $this->getMethod("save_content");
 	
 	$content = "Unit Link Filter Load Save test with some <b>HTML</b>. Should be identical! <a href=\"http://www.boe.com\">Id</a> 
 		With enters and two <a href=\"link\">links</a>";
 	
 	$loadhtml->invokeArgs($f, array($content));
 	$result = $savehtml->invokeArgs($f,array());
 	      
 	$this->assertEqual($content,$result);      
 }
   
}

?>
