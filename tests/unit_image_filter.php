<?php

class ImageFilterSimpleTest extends UnitTestCase {

	var $upload_dir; 
	
	
	public function __construct() 
	{
		$dir = wp_upload_dir(); 
		$this->upload_dir = $dir["url"]; 
		
	}
	/* 
	Unit test private functions
	*/
	protected static function getMethod($name) {
	  $class = new ReflectionClass('faf_image_filter');
	  $method = $class->getMethod($name);
	  $method->setAccessible(true);
	  return $method;
	}

	function testGrabRemoteImage()
	{
		// Standard image
		$url = "http://unit-test.weblogmechanic.com/balzaka.jpg"; 
		$imgext = 'jpg';
		$method = $this->getMethod("grab_remote_image"); 
		$obj = new faf_image_filter(array(),array("imgext" => $imgext)); 
  		$result = $method->invokeArgs($obj, array($url,'balzaka.jpg'));
  		
  		$this->assertNotA($result,'integer'); 
  		$this->assertIsA($result,'string'); 
 
 		// Image with -_- and capitals
 		$url = "http://unit-test.weblogmechanic.com/IMG_8506_-_Version_2.jpg"; 
 		$result = $method->invokeArgs($obj, array($url,'IMG_8506_-_Version_2.jpg'));
  		$this->assertNotA($result,'integer'); 
  		$this->assertIsA($result,'string'); 
 
 		// This one failed once ( so include in standard test ) 
 		$url = "http://unit-test.weblogmechanic.com/jun-13-Skateboarding_Day-preview.jpg"; 
 		$result = $method->invokeArgs($obj, array($url,'jun-13-Skateboarding_Day-preview.jpg'));
   		$this->assertNotA($result,'integer'); 
  		$this->assertIsA($result,'string'); 
 
  		// This one failed once ( so include in standard test ) 
 		$url = "http://unit-test.weblogmechanic.com/jun-13-I%27d_rather_be_playing_cello-preview.png"; 
 		$result = $method->invokeArgs($obj, array($url,'jun-13-I%27d_rather_be_playing_cello-preview.png'));
   		$this->assertNotA($result,'integer'); 
  		$this->assertIsA($result,'string'); 
  		
  		// Flickr 
  		$url = "http://farm8.staticflickr.com/7293/8743566666_fcafe8e4b5.jpg"; 
  		$result = $method->invokeArgs($obj, array($url,'8743566666_fcafe8e4b5.jpg'));
   		$this->assertNotA($result,'integer'); 
  		$this->assertIsA($result,'string');  
  		
 
  		// + TEST 404 error on image 
  		$url = "http://unit-test.weblogmechanic.com/blergh.jpg"; 
  		try {
  		 $result = $method->invokeArgs($obj, array($url,'blergh.jpg'));
  		 
  		 }
  		catch (Exception $e) { $result = $e; } 
  		$this->assertIsA($result,'Exception');
  		$this->assertEqual($result->getMessage(),"404 - File not found");
  		// + TEST file exists on filesystem
  		
	}
	
 	function testImageProcess()
 	{
 		$url = "<img src=\"http://unit-test.weblogmechanic.com/jun-13-Skateboarding_Day-preview.jpg?w=10&h=100\" />";		
 		$post = array("post_content" => $url, 
 						"post_excerpt" => "", 
 						"post_title" => "Snowboard"
 			);
 			
 		$args = array("filter_image_local" => 1,
 					  "faf_image_resize" => 'medium'

 				); 
 		
 		$method = $this->getMethod("image_process"); 
		$obj = new faf_image_filter(array($post),array($args )); 
  		$result = $method->invokeArgs($obj, array($post,$args));
  		
  		//basszje_snowboard-308x300.jpg
		$this->assertIsA(htmlentities($result["post_content"]),'string'); 
		$this->assertEqual( htmlentities($result["post_content"]), htmlentities("<img src=\"" . $this->upload_dir . "/jun-13-Skateboarding_Day-preview-480x300.jpg\" width=\"480\" height=\"300\" alt=\"Snowboard\" />") );
		
		// width and height first in string
		$url = '<img width="500" height="281" alt="Skateboarding Day" src="http://unit-test.weblogmechanic.com/jun-13-Skateboarding_Day-preview.jpg" />';
		$post["post_content"] = $url; 
		$result = $method->invokeArgs($obj, array($post,$args));
		$this->assertIsA(htmlentities($result["post_content"]),'string'); 
		$this->assertEqual(htmlentities($result["post_content"]), htmlentities("<img src=\"" . $this->upload_dir . "/jun-13-Skateboarding_Day-preview-480x300.jpg\" width=\"480\" height=\"300\" alt=\"Snowboard\" />") );		
		
		// Bad formed URL, failed once
		$url = '<img width="500" height="281" alt="I\'d rather be playing cello." src="http://unit-test.weblogmechanic.com/jun-13-I%27d_rather_be_playing_cello-preview.png" />';
	$post["post_content"] = $url; 
		$result = $method->invokeArgs($obj, array($post,$args));
		$this->assertIsA(htmlentities($result["post_content"]),'string'); 
		$this->assertEqual(htmlentities($result["post_content"]), htmlentities("<img src=\"" . $this->upload_dir . "/jun-13-I%27d_rather_be_playing_cello-preview.png\" width=\"500\" height=\"281\" alt=\"Snowboard\" />") );		
		
		// In Excerpt
		$url = '<img width="500" height="281" alt="I\'d rather be playing cello." src="http://unit-test.weblogmechanic.com/jun-13-I%27d_rather_be_playing_cello-preview.png" />';
	$post["post_content"] = "";
	$post["post_excerpt"] = $url;
		$result = $method->invokeArgs($obj, array($post,$args,"excerpt"));
		$this->assertIsA(htmlentities($result["post_excerpt"]),'string'); 
		$this->assertEqual(htmlentities($result["post_excerpt"]), htmlentities("<img src=\"" . $this->upload_dir . "/jun-13-I%27d_rather_be_playing_cello-preview.png\" width=\"500\" height=\"281\" alt=\"Snowboard\" />") );		
		
		// Failed before
		$url = '<img class="img-frontpage" alt="PEEN4936 - Version 2" src="http://unit-test.weblogmechanic.com/PEEN4936_-_Version_2.jpg" height="324" width="960" />';

	$post["post_content"] = "";
	$post["post_excerpt"] = $url;
		$result = $method->invokeArgs($obj, array($post,$args,"excerpt"));
		$this->assertIsA(htmlentities($result["post_excerpt"]),'string'); 
		$this->assertEqual(htmlentities($result["post_excerpt"]), htmlentities("<img src=\"" . $this->upload_dir . "/PEEN4936_-_Version_2-545x183.jpg\" width=\"545\" height=\"183\" alt=\"Snowboard\" />") );				
		
		// Failed before ( with link ) 
		$post["post_content"] = '<a target="_self" rel="nofollow" href="http://abcnewsradioonline.com/sports-news/tigers-advance-to-alcs.html"><img src="http://abcnewsradioonline.com/storage/sports-news-images/Getty_S_4912_MLB%20Baseball%201.jpg?__SQUARESPACE_CACHEVERSION=1381464602915" alt=""></a>';
		$post["post_excerpt"] = ""; 
		
		$result = $method->invokeArgs($obj, array($post,$args,"excerpt"));
		$this->assertIsA(htmlentities($result["post_content"]),'string'); 
		$this->assertEqual(htmlentities($result["post_content"]), htmlentities("<img src=\"" . $this->upload_dir . "/Getty_S_4912_MLB%20Baseball%201.jpg\" width=\"630\" height=\"354\" alt=\"Snowboard\" />") );		
		

		
 	}
 	
 	function testEnclosures()
 	{
 		$post["meta"]["enclosure"][0] = "http://unit-test.weblogmechanic.com/PEEN4936_-_Version_2.jpg 
 		0 image/jpeg";
 	 		$args = array("filter_image_local" => 1,
 					  "faf_image_resize" => 'medium'
 				);	
 		$method = $this->getMethod("save_enclosure"); 
		$obj = new faf_image_filter(array($post),array($args ));  
 		$result = $method->invokeArgs($obj, array($post));	
 	
 		$expect = array($this->upload_dir . "/PEEN4936_-_Version_2.jpg 
 		0 image/jpeg"); 
 		$this->assertEqual($result["meta"]["enclosure"],$expect); 
 		
 		// empty enclosure
 		$post["meta"]["enclosure"] = ""; 
 		$result = $method->invokeArgs($obj, array($post));
 		$this->assertEqual($post["meta"]["enclosure"],$result["meta"]["enclosure"]);



 		
 		// multiple
 		$post["meta"]["enclosure"] = array("http://unit-test.weblogmechanic.com/PEEN4936_-_Version_2.jpg 
 		0 image/jpeg", 
 		"http://unit-test.weblogmechanic.com/PEEN4936_-_Version_2.jpg 
 		0 image/jpeg", 
 		"http://unit-test.weblogmechanic.com/PEEN4936_-_Version_2.jpg 
 		0 image/jpeg"); 
 		$result = $method->invokeArgs($obj, array($post));
 		$this->assertEqual( count($result["meta"]["enclosure"]), count($post["meta"]["enclosure"]) ); 
 		
 	}
 	
// <img src="http://localhost/zoetermeer/wp-content/uploads/2013/06/basszje_snowboard-308x300.jpg" width="308" height="300" alt="Snowboard" /> ]
// <img src="http://localhost/zoetermeer/wp-content/uploads/2013/06/basszje_snowboard-308x300.jpg" width="308" height="300" alt="Snowboard" />
 	
 	
	function testProcessComplete()
	{
	global $wpdb; 
	
	$method = $this->getMethod("process_complete"); 
	$obj = new faf_image_filter(array(),array()); 

	
	// insert stuff into DB 
	
	// Test Procress Attachment add
	$sql = "insert into $wpdb->postmeta (meta_key, meta_value, post_id) values ('faf_process_image','200','100')"; 
	$wpdb->query($sql);
	
	$result = $method->invokeArgs($obj, array());	
	
	// Test Process Set image Thumbnail
	$sql = "insert into $wpdb->postmeta (meta_key, meta_value, post_id) values ('faf_featured_image','200','100')"; 
	$wpdb->query($sql);
		
	$result = $method->invokeArgs($obj, array());	
	//$this->assertTrue(has_post_thumbnail(100)); // fails because of fictional post_id value
	
	// todo : 
	
	}

	function testRemoveUnwantedImages()
	{
		$content = "<img src='http://blabla1'><img src='http://blabla2'><img src='http://blabla3'>"; 
		preg_match_all( '/<img[^>]+src\s*=\s*["\']?([^"\' ]+)[^>]*>/i', $content, $matches, PREG_SET_ORDER );
		$image_process = array(1,3); 
		$remove_unselected = 0; 

		
		$method = $this->getMethod("remove_unwanted_images"); 
		$obj = new faf_image_filter(array(),array()); 
		list ($return_content, $return_matches) = $method->invokeArgs($obj, array($content, $matches, $image_process,$remove_unselected)); 
		
		// Image process yes, no removing 
		$this->assertEqual($content,$return_content); 
		$this->assertNotEqual($matches,$return_matches); 
		$this->assertEqual(count($return_matches),2); 
		
		// Image process yes, remove yes 
		$remove_unselected = 1;
		list ($return_content, $return_matches) = $method->invokeArgs($obj, array($content, $matches, $image_process,$remove_unselected)); 
		
		$this->assertNotEqual($content,$return_content); 
		$this->assertEqual($return_content, "<img src='http://blabla1'><img src='http://blabla3'>");
		$this->assertNotEqual($matches,$return_matches); 
		$this->assertEqual(count($return_matches),2); 	
		
		$image_process = array(); 
		list ($return_content, $return_matches) = $method->invokeArgs($obj, array($content, $matches, $image_process,$remove_unselected)); 
		
		// no processing should be done
		$this->assertEqual($content,$return_content); 
		$this->assertEqual($matches,$return_matches); 
		$this->assertEqual(count($return_matches),3);

		$image_process = array(" "); 
		list ($return_content, $return_matches) = $method->invokeArgs($obj, array($content, $matches, $image_process,$remove_unselected)); 
		
		// no processing should be done
		$this->assertEqual($content,$return_content); 
		$this->assertEqual($matches,$return_matches); 
		$this->assertEqual(count($return_matches),3);

		$image_process = array("what","no",4); // faulty input
		list ($return_content, $return_matches) = $method->invokeArgs($obj, array($content, $matches, $image_process,$remove_unselected)); 
		
		// no processing should be done
		$this->assertEqual($content,$return_content); 
		$this->assertEqual($matches,$return_matches); 
		$this->assertEqual(count($return_matches),3);		 		
	}

	function testRemoveExcerpt()
	{
		$content = "Content<img src=\"http://localhost/rss_images/jun-13-Skateboarding_Day-preview.jpg?w=10&h=100\" />Content";
		$post = array("post_excerpt" => $content);  

		
		// basic remove
		$method = $this->getMethod("remove_image_excerpt"); 
		$obj = new faf_image_filter($post, array()); 
		$result =$method->invokeArgs($obj, array($post));
		$this->assertEqual($result["post_excerpt"],"ContentContent"); 
		
		// other order
		$content = 'Content<img width="500" height="281" alt="Skateboarding Day" src="http://localhost/rss_images/jun-13-Skateboarding_Day-preview.jpg" />Content'; 
		$post["post_excerpt"] = $content; 
		
		$result =$method->invokeArgs($obj, array($post));
		$this->assertEqual($result["post_excerpt"],"ContentContent"); 
		
		// multiple
		$content = "Con<img src='http://blabla1'>tent<img src='http://blabla2' alt='Content'>Content<img src='http://blabla3'>"; 	
				$post["post_excerpt"] = $content; 
		$result =$method->invokeArgs($obj, array($post));
		$this->assertEqual($result["post_excerpt"],"ContentContent"); 
	}
}
?>
